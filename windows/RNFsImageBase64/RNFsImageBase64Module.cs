using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Fs.Image.Base64.RNFsImageBase64
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNFsImageBase64Module : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNFsImageBase64Module"/>.
        /// </summary>
        internal RNFsImageBase64Module()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNFsImageBase64";
            }
        }
    }
}
