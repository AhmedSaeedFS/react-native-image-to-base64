package com.fetchsky.RNImageBase64;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;

public class RNFsImageBase64Module extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  public RNFsImageBase64Module(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @Override
  public String getName() {
    return "RNImgToBase64";
  }

  @ReactMethod
  public void getBase64String(String uri, final Promise promise) {
    try {
      URL url = new URL(uri);
      Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        if (image == null) {
        promise.reject("Error", "Failed to decode Bitmap, uri: " + uri);
      } else {
        promise.resolve(bitmapToBase64(image));
      }
    } catch (IOException e) {
//      Log.i("image###", e.toString());
        promise.reject("Error", "Failed to decode Bitmap, uri: " + uri);
    }
  }

  private String bitmapToBase64(Bitmap bitmap) {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
    byte[] byteArray = byteArrayOutputStream.toByteArray();
    return Base64.encodeToString(byteArray, Base64.DEFAULT);
  }
}